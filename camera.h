//
// Created by sinn on 2/8/20.
//

#ifndef LEARNOPENGL_CAMERA_H
#define LEARNOPENGL_CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>

enum class CAMERA_MOVEMENT {
	FORWARD, BACKWARD, LEFT, RIGHT
};

const float YAW = -90.0f;
const float PITCH = 0.0f;
const float ROLL = 0.0f;
const float SPEED = 5.0f;
const float SENSITIVITY = 0.22f;
const float ZOOM = 45.0f;

class Camera {
public:
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;
	float yaw;
	float pitch;
	float roll;
	float moveSpeed;
	float moveSensitivity;
	float zoom;

	Camera(glm::vec3 pos=glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3 worldUp=glm::vec3(0.0f, 1.0f, 0.0f),
			float yaw=YAW,
			float pitch=PITCH,
			float roll=ROLL)
			: front{glm::vec3(0.0f, 0.0f, -1.0f)}, moveSpeed(SPEED), moveSensitivity{SENSITIVITY}, zoom{ZOOM} {
		this->position = pos;
		this->worldUp = worldUp;
		this->yaw = yaw;
		this->pitch = pitch;
		this->roll = roll;
		updateCameraVectors();
	}

	Camera(float x, float y, float z,
				 float upx, float upy, float upz,
				 float yaw=YAW,
				 float pitch=PITCH,
				 float roll=ROLL)
			: front{glm::vec3(0.0f, 0.0f, -1.0f)}, moveSpeed(SPEED), moveSensitivity{SENSITIVITY}, zoom{ZOOM} {
		this->position = glm::vec3(x, y, z);
		this->worldUp = glm::vec3(upx, upy, upz);
		this->yaw = yaw;
		this->pitch = pitch;
		this->roll = roll;
		updateCameraVectors();
	}

	glm::mat4 getViewMatrix() {
		return glm::lookAt(position, position + front, up);
	}

	void processKeyBoard(CAMERA_MOVEMENT direction, float deltaTime) {
		float velocity = moveSpeed * deltaTime;
		if (direction == CAMERA_MOVEMENT::FORWARD) {
			position += front * velocity;
		}
		if (direction == CAMERA_MOVEMENT::BACKWARD) {
			position -= front * velocity;
		}
		if (direction == CAMERA_MOVEMENT::LEFT) {
			position -= right * velocity;
		}
		if (direction == CAMERA_MOVEMENT::RIGHT) {
			position += right * velocity;
		}
	}

	void processMouseMovement(float xOffset, float yOffset, bool constrainPitch=true) {
		xOffset *= moveSensitivity;
		yOffset *= moveSensitivity;
		yaw += xOffset;
		pitch += yOffset;
		if (constrainPitch) {
			pitch = fmin(pitch, 89.5f);
			pitch = fmax(pitch, -89.5f);
		}
		updateCameraVectors();
	}

	void processMouseScroll(float yOffset) {
		zoom -= yOffset;
		zoom = fmin(zoom, ZOOM);
		zoom = fmax(zoom, 1.0f);
	}

private:
	void updateCameraVectors() {
		glm::vec3 f;
		f.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		f.y = sin(glm::radians(pitch));
		f.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		front = glm::normalize(f);
		right = glm::normalize(glm::cross(front, worldUp));
		up = glm::normalize(glm::cross(right, front));
	}
};


#endif //LEARNOPENGL_CAMERA_H
