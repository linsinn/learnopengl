//
// Created by Sinn on 2/16/2020.
//

#ifndef LEARNOPENGL_MODEL_H
#define LEARNOPENGL_MODEL_H

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "shader.h"
#include "mesh.h"
#include "utils.h"

#include <string>
#include <vector>
#include <unordered_set>

using std::string;
using std::vector;

class Model {
public:
	Model(const string &path);

	void draw(const Shader &shader);

private:
	vector<Mesh> meshes;
	string directory;
	std::unordered_set<string> textures_loaded;

	void loadModel(const string &path);

	void processNode(const aiNode *node, const aiScene *scene);

	Mesh processMesh(const aiMesh *mesh, const aiScene *scene);

	vector<Texture> loadMaterialTextures(const aiMaterial *mat, const aiTextureType &type);
};

Model::Model(const string &path) {
	loadModel(path);
}

void Model::loadModel(const string &path) {
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(path.c_str(), aiProcess_Triangulate | aiProcess_FlipUVs);
	if (!scene || (scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) || !scene->mRootNode) {
		std::cerr << "ERROR::ASSIMP::" << importer.GetErrorString() << '\n';
		return;
	}
	directory = path.substr(0, path.find_last_of('/'));
	processNode(scene->mRootNode, scene);
}

void Model::processNode(const aiNode *node, const aiScene *scene) {
	for (int i = 0; i < node->mNumMeshes; ++i) {
		aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.emplace_back(processMesh(mesh, scene));
	}
	for (int i = 0; i < node->mNumChildren; ++i) {
		processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(const aiMesh *mesh, const aiScene *scene) {
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	vector<Texture> textures;
	for (int i = 0; i < mesh->mNumVertices; ++i) {
		Vertex vertex;
		vertex.position = {mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z};
		vertex.normal = {mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z};
		if (mesh->HasTextureCoords(0)) {
			vertex.texCoords = {mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y};
		} else {
			vertex.texCoords = {0.0f, 0.0f};
		}
		vertices.emplace_back(vertex);
	}
	for (int i = 0; i < mesh->mNumFaces; ++i) {
		const aiFace &face = mesh->mFaces[i];
		for (int j = 0; j < face.mNumIndices; ++j) {
			indices.emplace_back(face.mIndices[j]);
		}
	}
	if (mesh->mMaterialIndex >= 0) {
		const aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE);
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR);
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}
	return Mesh(vertices, indices, textures);
}

vector<Texture> Model::loadMaterialTextures(const aiMaterial *mat, const aiTextureType &type) {
	vector<Texture> textures;
	for (int i = 0; i < mat->GetTextureCount(type); ++i) {
		aiString str;
		mat->GetTexture(type, i, &str);
		if (textures_loaded.find(str.C_Str()) == textures_loaded.end()) {
			textures_loaded.emplace(str.C_Str());
			Texture texture;
			texture.id = textureFromFile(str.C_Str(), directory);
			texture.type = type;
			texture.path = str.C_Str();
			textures.emplace_back(texture);
		}
	}
	return textures;
}

void Model::draw(const Shader &shader) {
	for (Mesh &mesh: meshes) {
		mesh.draw(shader);
	}
}

#endif //LEARNOPENGL_MODEL_H
