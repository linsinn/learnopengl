//
// Created by Sinn on 2/7/2020.
//

#ifndef LEARNOPENGL_SHADER_H
#define LEARNOPENGL_SHADER_H

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "utils.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader {
public:
	// the program ID
	unsigned int shaderProgramId;

	// constructor reads and builds the shader
	Shader(std::string vertexPath, std::string fragmentPath);

	// use/activate the shader
	void use();

	// utility uniform functions
	inline void setBool(const std::string &name, bool value) const;

	inline void setInt(const std::string &name, int value) const;

	inline void setFloat(const std::string &name, float value) const;

	inline void setMat4(const std::string &name, const glm::mat4 &value) const;

	inline void setVec3(const std::string &name, const glm::vec3& vec) const;
	inline void setVec3(const std::string &name, float x, float y, float z) const;
};

Shader::Shader(std::string vertexPath, std::string fragmentPath) {
	vertexPath = realPath(vertexPath);
	fragmentPath = realPath(fragmentPath);
	// 1. read vertex/fragment shader source code from file
	std::string vertexCode, fragmentCode;
	std::ifstream shaderFile;
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		std::stringstream readStream;
		shaderFile.open(vertexPath);
		readStream << shaderFile.rdbuf();
		shaderFile.close();
		vertexCode = readStream.str();
	} catch (const std::ifstream::failure &err) {
		std::cerr << "ERROR::SHADER::VERTEX_SHADER_FAILED_TO_READ\n" << err.what() << '\n';
	}
	try {
		std::stringstream readStream;
		readStream.clear();
		shaderFile.open(fragmentPath);
		readStream << shaderFile.rdbuf();
		shaderFile.close();
		fragmentCode = readStream.str();
	} catch (const std::ifstream::failure &err) {
		std::cerr << "ERROR::SHADER::FRAGMENT_SHADER_FAILED_TO_READ\n" << err.what() << '\n';
	}

	// 2. compile shaders
	auto vShaderCode = vertexCode.c_str();
	auto fShaderCode = fragmentCode.c_str();

	int success;
	char infoLog[512];

	// vertex shader
	unsigned int vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &vShaderCode, nullptr);
	glCompileShader(vertex);
	glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertex, 512, nullptr, infoLog);
		std::cerr << "ERROR::SHADER::VERTEX::COMPILE_FAILED\n" << infoLog << '\n';
	}
	// fragment shader
	unsigned int fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &fShaderCode, nullptr);
	glCompileShader(fragment);
	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragment, 512, nullptr, infoLog);
		std::cerr << "ERROR::SHADER::FRAGMENT::COMPILE_FAILED\n" << infoLog << '\n';
	}
	// link shader
	shaderProgramId = glCreateProgram();
	glAttachShader(shaderProgramId, vertex);
	glAttachShader(shaderProgramId, fragment);
	glLinkProgram(shaderProgramId);
	glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgramId, 512, nullptr, infoLog);
		std::cerr << "ERROR::SHADER:PROGRAM::LINKING_FAILED\n" << infoLog << '\n';
	}

	// delete the shaders
	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

void Shader::use() {
	glUseProgram(shaderProgramId);
}

inline void Shader::setBool(const std::string &name, bool value) const {
	glUniform1i(glGetUniformLocation(shaderProgramId, name.c_str()), (int) value);
}

inline void Shader::setInt(const std::string &name, int value) const {
	glUniform1i(glGetUniformLocation(shaderProgramId, name.c_str()), value);
}

inline void Shader::setFloat(const std::string &name, float value) const {
	glUniform1f(glGetUniformLocation(shaderProgramId, name.c_str()), value);
}

inline void Shader::setMat4(const std::string &name, const glm::mat4 &value) const {
	glUniformMatrix4fv(glGetUniformLocation(shaderProgramId, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

inline void Shader::setVec3(const std::string &name, float x, float y, float z) const {
	glUniform3f(glGetUniformLocation(shaderProgramId, name.c_str()), x, y, z);
}

inline void Shader::setVec3(const std::string &name, const glm::vec3 &vec) const {
	setVec3(name, vec.x, vec.y, vec.z);
}

#endif //LEARNOPENGL_SHADER_H
